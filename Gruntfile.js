'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
      test: {
        options: {},
        files: {
          // 'test/pre.css': 'src/pre.less'
        }
      }
    },
    watch: {
      test: {
        files: ['test/*.less'],
        tasks: ['less:test']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');


  grunt.registerTask('default', ['less:test']);
};